/*Copyright (c) 2017, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.*/
#include <gst/gst.h>
#include <glib.h>
#include <glib/gthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>

typedef struct _pipeline_input{
  GstElement *pipeline;
  int qcarcamsrc_input;
  GstElement *tee;
  GstElement *queue1;
  GstElement *queue2;
  GstElement *capsfilter;
  GstElement *omxh264enc;
  char* location;
  int port;
}pipelineinput;

typedef void (*GstMultiSendKbFunc) (const gchar * kb_input, gpointer user_data);

pipelineinput pipeline_input[4]={0};
static struct termios term_settings;
static gboolean term_settings_saved = FALSE;
static GstMultiSendKbFunc kb_callback;
static gpointer kb_callback_data;
static gulong io_watch_id;
gchar *buf;
guint framerate_buf[] = {30,29,28,27,26,25,24};
guint bitrate_buf[] = {8000000,7700000,7400000,7100000,6800000,6500000,6200000};

static gboolean
gst_multisend_kb_io_cb (GIOChannel * ioc, GIOCondition cond, gpointer user_data)
{
  GIOStatus status;

  if (cond & G_IO_IN) {
    gsize read;

    status = g_io_channel_read_line_string (ioc, &buf, &read, NULL);
    if (status == G_IO_STATUS_ERROR)
      return FALSE;
    if (status == G_IO_STATUS_NORMAL) {
      if (kb_callback)
        kb_callback (buf, kb_callback_data);
    }
  }

  return TRUE;
}

gboolean
gst_multisend_kb_set_key_handler (GstMultiSendKbFunc kb_func, gpointer user_data)
{
  GIOChannel *ioc;
  int flags;
  
  g_print("gst_multisend_kb_set_key_handler\n");
  if (!isatty (STDIN_FILENO)) {
    g_print ("stdin is not connected to a terminal\n");
    return FALSE;
  }
  g_print("gst_multisend_kb_set_key_handler 1\n");

  if (io_watch_id > 0) {
    g_source_remove (io_watch_id);
    io_watch_id = 0;
  }
  g_print("gst_multisend_kb_set_key_handler 2\n");

  if (kb_func == NULL && term_settings_saved) {

    if (tcsetattr (STDIN_FILENO, TCSAFLUSH, &term_settings) == 0)
      term_settings_saved = FALSE;
    else
      g_print ("could not restore terminal attributes\n");

    setvbuf (stdin, NULL, _IOLBF, 0);
  }
  g_print("gst_multisend_kb_set_key_handler 3\n");
  if (kb_func != NULL) {
    struct termios new_settings;

    if (!term_settings_saved) {
      if (tcgetattr (STDIN_FILENO, &term_settings) != 0) {
        g_print ("could not save terminal attributes\n");
        return FALSE;
      }
      term_settings_saved = TRUE;
      g_print("gst_multisend_kb_set_key_handler 4\n");

      new_settings = term_settings;
      new_settings.c_lflag &= ~(ECHO | ICANON | IEXTEN);

      if (tcsetattr (STDIN_FILENO, TCSAFLUSH, &new_settings) != 0) {
        g_print ("Could not set terminal state\n");
        return FALSE;
      }
      setvbuf (stdin, NULL, _IONBF, 0);
    }
  }
  g_print("gst_multisend_kb_set_key_handler 5\n");

  ioc = g_io_channel_unix_new (STDIN_FILENO);

  flags = g_io_channel_get_flags (ioc);
  g_io_channel_set_flags (ioc, flags | G_IO_FLAG_NONBLOCK, NULL);

  io_watch_id = g_io_add_watch_full (ioc, G_PRIORITY_DEFAULT, G_IO_IN,
      (GIOFunc) gst_multisend_kb_io_cb, user_data, NULL);
  g_print("gst_multisend_kb_set_key_handler 6\n");
  g_io_channel_unref (ioc);

  kb_callback = kb_func;
  kb_callback_data = user_data;

  return TRUE;
}



static gboolean
bus_call (GstBus * bus, GstMessage * msg, gpointer data)
{
  GMainLoop *loop = (GMainLoop *) data;

  switch (GST_MESSAGE_TYPE (msg)) {
    case GST_MESSAGE_EOS:{
      g_print ("End-of-stream\n");
      g_main_loop_quit (loop);
      break;
    }
    case GST_MESSAGE_ERROR:{
      gchar *debug;
      GError *err;

      gst_message_parse_error (msg, &err, &debug);
      g_printerr ("Debugging info: %s\n", (debug) ? debug : "none");
      g_free (debug);

      g_print ("Error: %s\n", err->message);
      g_error_free (err);

      g_main_loop_quit (loop);

      break;
    }
    default:
      break;
  }
  return TRUE;
}

gint create_pipeline(pipelineinput *input)
{
  GstElement *qcarcamsrc;
  GstElement *capsfilter;
  GstElement *queue;
  GstElement *videorate1;
  GstElement *qvconv1;
  GstElement *capsfilter12;
  GstElement *rtph264pay1;
  GstElement *udpsink1;
  GstElement *videorate2;
  GstElement *qvconv2;
  GstElement *capsfilter2;
  GstElement *clockoverlay2;
  GstElement *omxh264enc2;
  GstElement *avimux2;
  GstElement *filesink2;
  
  input->pipeline = gst_pipeline_new ("pipeline");
  qcarcamsrc = gst_element_factory_make ("qcarcamsrc", NULL);
  if (!qcarcamsrc) {
    g_print ("'qcarcamsrc' gstreamer plugin missing\n");
    return 1;
  }
  g_object_set (qcarcamsrc, "input", input->qcarcamsrc_input, "dumpable", TRUE, NULL);
  g_print("qcarcamsrc input %d\n", input->qcarcamsrc_input);

  capsfilter = gst_element_factory_make ("capsfilter", NULL);
  if (!capsfilter) {
    g_print ("'capsfilter' gstreamer plugin missing\n");
    return 1;
  }
  g_object_set (capsfilter, "caps",gst_caps_new_simple ("video/x-raw",
				     "format", G_TYPE_STRING, "NV12",
				     "width", G_TYPE_INT, 1280,
				     "height", G_TYPE_INT, 720,
				     "framerate", GST_TYPE_FRACTION, 30, 1,
				     NULL), NULL);
				     
  queue = gst_element_factory_make ("queue", NULL);
  if (!queue) {
    g_print ("'queue' gstreamer plugin missing\n");
    return 1;
  }
  input->tee = gst_element_factory_make ("tee", NULL);
  if (!input->tee) {
    g_print ("'tee' gstreamer plugin missing\n");
    return 1;
  }
  input->queue1 = gst_element_factory_make ("queue", NULL);
  if (!input->queue1) {
    g_print ("'queue' gstreamer plugin missing\n");
    return 1;
  }
  videorate1 = gst_element_factory_make ("videorate", NULL);
  if (!videorate1) {
    g_print ("'videorate' gstreamer plugin missing\n");
    return 1;
  }
  input->capsfilter = gst_element_factory_make ("capsfilter", NULL);
  if (!input->capsfilter) {
    g_print ("'capsfilter' gstreamer plugin missing\n");
    return 1;
  }
  g_object_set (input->capsfilter, "caps",gst_caps_new_simple ("video/x-raw",
				     "format", G_TYPE_STRING, "NV12",
				     "framerate", GST_TYPE_FRACTION, 30, 1,
				     NULL), NULL);
  qvconv1 = gst_element_factory_make ("qvconv", NULL);
  if (!qvconv1) {
    g_print ("'qvconv' gstreamer plugin missing\n");
    return 1;
  }
  capsfilter12 = gst_element_factory_make ("capsfilter", NULL);
  if (!capsfilter12) {
    g_print ("'capsfilter' gstreamer plugin missing\n");
    return 1;
  }
  g_object_set (capsfilter12, "caps",gst_caps_new_simple ("video/x-raw",
				     "format", G_TYPE_STRING, "NV12",
				     "width", G_TYPE_INT, 352,
				     "height", G_TYPE_INT, 288,
				     NULL), NULL);
				     
  input->omxh264enc = gst_element_factory_make ("omxh264enc", NULL);
  if (!input->omxh264enc) {
    g_print ("'omxh264enc' gstreamer plugin missing\n");
    return 1;
  }
  g_object_set (input->omxh264enc, "target-bitrate", 8000000, NULL);
  rtph264pay1 = gst_element_factory_make ("rtph264pay", NULL);
  if (!rtph264pay1) {
    g_print ("'rtph264pay' gstreamer plugin missing\n");
    return 1;
  }
  udpsink1 = gst_element_factory_make ("udpsink", NULL);
  if (!udpsink1) {
    g_print ("'udpsink' gstreamer plugin missing\n");
    return 1;
  }
  g_object_set (udpsink1, "host", "127.0.0.1", "port", input->port, NULL);
  g_print("port %d\n", input->port);

  input->queue2 = gst_element_factory_make ("queue", NULL);
  if (!input->queue2) {
    g_print ("'queue' gstreamer plugin missing\n");
    return 1;
  }
  videorate2 = gst_element_factory_make ("videorate", NULL);
  if (!videorate2) {
    g_print ("'videorate' gstreamer plugin missing\n");
    return 1;
  }
  qvconv2 = gst_element_factory_make ("qvconv", NULL);
  if (!qvconv2) {
    g_print ("'qvconv' gstreamer plugin missing\n");
    return 1;
  }
  capsfilter2 = gst_element_factory_make ("capsfilter", NULL);
  if (!capsfilter2) {
    g_print ("'capsfilter' gstreamer plugin missing\n");
    return 1;
  }
  g_object_set (capsfilter2, "caps",gst_caps_new_simple ("video/x-raw",
				     "format", G_TYPE_STRING, "NV12",
				     "width", G_TYPE_INT, 720,
				     "height", G_TYPE_INT, 480,
				     "framerate", GST_TYPE_FRACTION, 1, 1,
				     NULL), NULL);
				     
  clockoverlay2 = gst_element_factory_make ("clockoverlay", NULL);
  if (!clockoverlay2) {
    g_print ("'clockoverlay' gstreamer plugin missing\n");
    return 1;
  }
  g_object_set (clockoverlay2, "time-format", "%H:%M:%S %Y/%m/%d", NULL);
  omxh264enc2 = gst_element_factory_make ("omxh264enc", NULL);
  if (!omxh264enc2) {
    g_print ("'omxh264enc' gstreamer plugin missing\n");
    return 1;
  }
  avimux2 = gst_element_factory_make ("avimux", NULL);
  if (!avimux2) {
    g_print ("'avimux' gstreamer plugin missing\n");
    return 1;
  }
  filesink2 = gst_element_factory_make ("filesink", NULL);
  if (!filesink2) {
    g_print ("'filesink' gstreamer plugin missing\n");
    return 1;
  }
  g_object_set (filesink2, "location", input->location, NULL);
  g_print("location %s\n", input->location);

  gst_bin_add_many (GST_BIN (input->pipeline), qcarcamsrc, capsfilter, queue,
      input->tee, input->queue1, videorate1, input->capsfilter, qvconv1, capsfilter12, input->omxh264enc,
      rtph264pay1, udpsink1, input->queue2,
      videorate2, qvconv2, capsfilter2,
      clockoverlay2, omxh264enc2, avimux2,
      filesink2,NULL);
      
  if(!gst_element_link (qcarcamsrc, capsfilter)){
    g_print("element link error qcarcamsrc\n");
  }
  if(!gst_element_link (capsfilter, queue)){
    g_print("element link error capsfilter\n");
  }
  if(!gst_element_link (queue, input->tee)){
    g_print("element link error queue\n");
  }
  
  if(!gst_element_link (input->queue1, videorate1)){
    g_print("element link error queue1\n");
  }
  if(!gst_element_link (videorate1, input->capsfilter)){
    g_print("element link error videorate1\n");
  }
  if(!gst_element_link (input->capsfilter, qvconv1)){
    g_print("element link error capsfilter11\n");
  }
  if(!gst_element_link (qvconv1, capsfilter12)){
    g_print("element link error qvconv1\n");
  }
  if(!gst_element_link (capsfilter12, input->omxh264enc)){
    g_print("element link error capsfilter1\n");
  }
  if(!gst_element_link (input->omxh264enc, rtph264pay1)){
    g_print("element link error omxh264enc\n");
  }
  if(!gst_element_link (rtph264pay1, udpsink1)){
    g_print("element link error rtph264pay1\n");
  }
  
  if(!gst_element_link (input->queue2, videorate2)){
    g_print("element link error queue2\n");
  }
  if(!gst_element_link (videorate2, qvconv2)){
    g_print("element link error videorate2\n");
  }
  if(!gst_element_link (qvconv2, capsfilter2)){
    g_print("element link error qvconv2\n");
  }
  if(!gst_element_link (capsfilter2, clockoverlay2)){
    g_print("element link error capsfilter2\n");
  }
  if(!gst_element_link (clockoverlay2, omxh264enc2)){
    g_print("element link error clockoverlay2\n");
  }
  if(!gst_element_link (omxh264enc2, avimux2)){
    g_print("element link error omxh264enc2\n");
  }
  if(!gst_element_link (avimux2, filesink2)){
    g_print("element link error avimux2\n");
  }
}

void* func(void* data)
{
  g_main_loop_run (data);
}
gboolean handle_keyboard (const gchar * key_input, gpointer user_data) {   
  gint channel = 0;
  guint bitrate = 0;
  guint framerate = 0;
  int i = 0;
  int j = 0;
  FILE *fd = 0;
  int len = strlen(key_input);

  for(i=0; i<len; i++){
    if(key_input[i] != 32){
      break;
    }
  }

  for(j=i;j<len-i; j++){
    if(key_input[j] == 32){
      key_input[j] == '\0';
      break;
    }
  }
  channel = atoi(key_input+i);
  g_print("channel %d\n", channel);
  for(i=j+1; i<len; i++){
    if(key_input[i] != 32){
      break;
    }
  }

  for(j=i;j<len-i; j++){
    if(key_input[j] == 32){
      key_input[j] == '\0';
      break;
    }
  }
  bitrate = atoi(key_input+i);
  g_print("bitrate %d\n",bitrate);
  for(i=j+1; i<len; i++){
    if(key_input[i] != 32){
      break;
    }
  }

  for(j=i;j<len-i; j++){
    if(key_input[j] == 32){
      key_input[j] == '\0';
      break;
    }
  }
  framerate = atoi(key_input+i);
  g_print("framerate %d\n",framerate);

  if(framerate < 24 && framerate != 0) {
    g_print("suggest framerate bigger than 23\n");
  }
  
  fd = fopen("/home/root/bitrate","r");
  if(fd){
    fscanf(fd, "%d %d %d %d %d %d %d", &bitrate_buf[0],&bitrate_buf[1],&bitrate_buf[2],
        &bitrate_buf[3],&bitrate_buf[4], &bitrate_buf[5],&bitrate_buf[6]);
    fclose(fd);
  } 
  g_print("bitrate %d,%d,%d,%d,%d,%d,%d\n",bitrate_buf[0],bitrate_buf[1],bitrate_buf[2],
        bitrate_buf[3],bitrate_buf[4], bitrate_buf[5],bitrate_buf[6]);
         
  if(bitrate < bitrate_buf[6] && bitrate != 0) {
    g_print("suggest bitrate bigger than %d\n",bitrate_buf[6]);
  }
  if(framerate<=30 && framerate>=24 && bitrate>=bitrate_buf[6]){
    i = 0;
    j = 0;
    len = 7;
    for(i=0;i<len; i++){
      if(framerate == framerate_buf[i]){
        break;
      }
    }

    if(bitrate < bitrate_buf[i]){
      for(j=0;j<len;j++){
        if(bitrate >= bitrate_buf[j]){
           framerate=framerate_buf[j];
           j=j-1;
           g_print("for bitrate %d < %d, so set framerate to %d\n", bitrate, bitrate_buf[j], framerate);
           break;
        }
      }
    }
  }
  if(channel == 0){
    if(bitrate){
      g_object_set (pipeline_input[0].omxh264enc, "target-bitrate", bitrate, NULL);
    }
    if(framerate){
      g_object_set (pipeline_input[0].capsfilter, "caps",gst_caps_new_simple ("video/x-raw",
				     "format", G_TYPE_STRING, "NV12",
				     "framerate", GST_TYPE_FRACTION, framerate, 1,
				     NULL), NULL);
    }
  }
  if(channel == 1){
    if(bitrate){
      g_object_set (pipeline_input[1].omxh264enc, "target-bitrate", bitrate, NULL);
    }
    if(framerate){
      g_object_set (pipeline_input[1].capsfilter, "caps",gst_caps_new_simple ("video/x-raw",
				     "format", G_TYPE_STRING, "NV12",
				     "framerate", GST_TYPE_FRACTION, framerate, 1,
				     NULL), NULL);
    }
  }
  if(channel == 2){
    if(bitrate){
      g_object_set (pipeline_input[2].omxh264enc, "target-bitrate", bitrate, NULL);
    }
    if(framerate){
      g_object_set (pipeline_input[2].capsfilter, "caps",gst_caps_new_simple ("video/x-raw",
				     "format", G_TYPE_STRING, "NV12",
				     "framerate", GST_TYPE_FRACTION, framerate, 1,
				     NULL), NULL);
    }
  }

  if(channel == 3){
    if(bitrate){
      g_object_set (pipeline_input[3].omxh264enc, "target-bitrate", bitrate, NULL);
    }
    if(framerate){
      g_object_set (pipeline_input[3].capsfilter, "caps",gst_caps_new_simple ("video/x-raw",
				     "format", G_TYPE_STRING, "NV12",
				     "framerate", GST_TYPE_FRACTION, framerate, 1,
				     NULL), NULL);
    }
  }

}
gint
main (gint argc, gchar * argv[])
{
  GMainLoop *loop0;
  GstBus *bus0;
  guint bus_watch_id0;
  GMainLoop *loop1;
  GstBus *bus1;
  guint bus_watch_id1;
  GMainLoop *loop2;
  GstBus *bus2;
  guint bus_watch_id2;
  GMainLoop *loop3;
  GstBus *bus3;
  guint bus_watch_id3;
  GstPad *req_pad01;
  GstPad *req_pad02;
  GstPad *qpad01;
  GstPad *qpad02;
  GstPad *req_pad11;
  GstPad *req_pad12;
  GstPad *qpad11;
  GstPad *qpad12;
  GstPad *req_pad21;
  GstPad *req_pad22;
  GstPad *qpad21;
  GstPad *qpad22;
  GstPad *req_pad31;
  GstPad *req_pad32;
  GstPad *qpad31;
  GstPad *qpad32;
  GThread* p_thread0;
  GThread* p_thread1;
  GThread* p_thread2;
  GThread* p_thread3;
  
  gst_init (&argc, &argv);
  g_print("init\n");
  pipeline_input[0].qcarcamsrc_input = 0;
  pipeline_input[0].port=5000;
  pipeline_input[0].location="test_0.avi";
  pipeline_input[1].qcarcamsrc_input = 1;
  pipeline_input[1].port=5001;
  pipeline_input[1].location="test_1.avi";
  pipeline_input[2].qcarcamsrc_input = 2;
  pipeline_input[2].port=5002;
  pipeline_input[2].location="test_2.avi";
  pipeline_input[3].qcarcamsrc_input = 3;
  pipeline_input[3].port=5003;
  pipeline_input[3].location="test_3.avi";
  
  create_pipeline(&pipeline_input[0]);
  create_pipeline(&pipeline_input[1]);
  create_pipeline(&pipeline_input[2]);
  create_pipeline(&pipeline_input[3]);

  req_pad01 = gst_element_get_request_pad (pipeline_input[0].tee, "src_%u");
  req_pad02 = gst_element_get_request_pad (pipeline_input[0].tee, "src_%u");

  qpad01 = gst_element_get_static_pad (pipeline_input[0].queue1, "sink");
  qpad02 = gst_element_get_static_pad (pipeline_input[0].queue2, "sink");
  if(gst_pad_link (req_pad01, qpad01) != GST_PAD_LINK_OK){
    g_print("pad link error 01\n");
  }
  if(gst_pad_link (req_pad02, qpad02) != GST_PAD_LINK_OK){
    g_print("pad link error 02\n");
  }
  req_pad11 = gst_element_get_request_pad (pipeline_input[1].tee, "src_%u");
  req_pad12 = gst_element_get_request_pad (pipeline_input[1].tee, "src_%u");

  qpad11 = gst_element_get_static_pad (pipeline_input[1].queue1, "sink");
  qpad12 = gst_element_get_static_pad (pipeline_input[1].queue2, "sink");
  if(gst_pad_link (req_pad11, qpad11) != GST_PAD_LINK_OK){
    g_print("pad link error 11\n");
  }
  if(gst_pad_link (req_pad12, qpad12) != GST_PAD_LINK_OK){
    g_print("pad link error 12\n");
  }
  req_pad21 = gst_element_get_request_pad (pipeline_input[2].tee, "src_%u");
  req_pad22 = gst_element_get_request_pad (pipeline_input[2].tee, "src_%u");

  qpad21 = gst_element_get_static_pad (pipeline_input[2].queue1, "sink");
  qpad22 = gst_element_get_static_pad (pipeline_input[2].queue2, "sink");
  if(gst_pad_link (req_pad21, qpad21) != GST_PAD_LINK_OK){
    g_print("pad link error 21\n");
  }
  if(gst_pad_link (req_pad22, qpad22) != GST_PAD_LINK_OK){
    g_print("pad link error 22\n");
  }
  req_pad31 = gst_element_get_request_pad (pipeline_input[3].tee, "src_%u");
  req_pad32 = gst_element_get_request_pad (pipeline_input[3].tee, "src_%u");

  qpad31 = gst_element_get_static_pad (pipeline_input[3].queue1, "sink");
  qpad32 = gst_element_get_static_pad (pipeline_input[3].queue2, "sink");
  if(gst_pad_link (req_pad31, qpad31) != GST_PAD_LINK_OK){
    g_print("pad link error 31\n");
  }
  if(gst_pad_link (req_pad32, qpad32) != GST_PAD_LINK_OK){
    g_print("pad link error 32\n");
  }

  /* create and event loop and feed gstreamer bus mesages to it */
  loop0 = g_main_loop_new (NULL, FALSE);

  bus0 = gst_element_get_bus (pipeline_input[0].pipeline);
  bus_watch_id0 = gst_bus_add_watch (bus0, bus_call, loop0);
  
  loop1 = g_main_loop_new (NULL, FALSE);

  bus1 = gst_element_get_bus (pipeline_input[1].pipeline);
  bus_watch_id1 = gst_bus_add_watch (bus1, bus_call, loop1);

  loop2 = g_main_loop_new (NULL, FALSE);

  bus2 = gst_element_get_bus (pipeline_input[2].pipeline);
  bus_watch_id2 = gst_bus_add_watch (bus2, bus_call, loop2);

  loop3 = g_main_loop_new (NULL, FALSE);
  bus3 = gst_element_get_bus (pipeline_input[3].pipeline);
  bus_watch_id3 = gst_bus_add_watch (bus3, bus_call, loop3);

  g_object_unref (bus0);
  g_object_unref (bus1);
  g_object_unref (bus2);
  g_object_unref (bus3);

  g_print("set key handle\n");
  gst_multisend_kb_set_key_handler(handle_keyboard, NULL);
  g_print("set key handle+++\n"); 

  /* start play back and listed to events */
  gst_element_set_state (pipeline_input[0].pipeline, GST_STATE_PLAYING);
  p_thread0 = g_thread_new("thread0", func, loop0);
  
  gst_element_set_state (pipeline_input[1].pipeline, GST_STATE_PLAYING);
  p_thread1 = g_thread_new("thread1", func, loop1);
 
  gst_element_set_state (pipeline_input[2].pipeline, GST_STATE_PLAYING);
  p_thread2 = g_thread_new("thread2", func, loop2);
 
  gst_element_set_state (pipeline_input[3].pipeline, GST_STATE_PLAYING);
  p_thread3 = g_thread_new("thread3", func, loop3);

  g_thread_join (p_thread0);
  g_thread_join (p_thread1);
  g_thread_join (p_thread2);
  g_thread_join (p_thread3);
  
  /* cleanup */
  gst_element_set_state (pipeline_input[0].pipeline, GST_STATE_NULL);
  gst_element_release_request_pad (pipeline_input[0].tee, req_pad01);
  gst_object_unref (req_pad01);
  gst_element_release_request_pad (pipeline_input[0].tee, req_pad02);
  gst_object_unref (req_pad02);
  gst_object_unref (qpad01);
  gst_object_unref (qpad02);
  
  g_object_unref (pipeline_input[0].pipeline);
  g_source_remove (bus_watch_id0);
  g_main_loop_unref (loop0);

  gst_element_set_state (pipeline_input[1].pipeline, GST_STATE_NULL);
  gst_element_release_request_pad (pipeline_input[1].tee, req_pad11);
  gst_object_unref (req_pad11);
  gst_element_release_request_pad (pipeline_input[1].tee, req_pad12);
  gst_object_unref (req_pad12);
  gst_object_unref (qpad11);
  gst_object_unref (qpad12);
  
  g_object_unref (pipeline_input[1].pipeline);
  g_source_remove (bus_watch_id1);
  g_main_loop_unref (loop1);

  gst_element_set_state (pipeline_input[2].pipeline, GST_STATE_NULL);
  gst_element_release_request_pad (pipeline_input[2].tee, req_pad21);
  gst_object_unref (req_pad21);
  gst_element_release_request_pad (pipeline_input[2].tee, req_pad22);
  gst_object_unref (req_pad22);
  gst_object_unref (qpad21);
  gst_object_unref (qpad22);
  
  g_object_unref (pipeline_input[2].pipeline);
  g_source_remove (bus_watch_id2);
  g_main_loop_unref (loop2);
  
  gst_element_set_state (pipeline_input[3].pipeline, GST_STATE_NULL);
  gst_element_release_request_pad (pipeline_input[3].tee, req_pad31);
  gst_object_unref (req_pad31);
  gst_element_release_request_pad (pipeline_input[3].tee, req_pad32);
  gst_object_unref (req_pad32);
  gst_object_unref (qpad31);
  gst_object_unref (qpad32);
  
  g_object_unref (pipeline_input[3].pipeline);
  g_source_remove (bus_watch_id3);
  g_main_loop_unref (loop3);
  
  return 0;
}
